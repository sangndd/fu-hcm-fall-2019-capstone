// const express = require('express');
const Visitor = require("../models/Visitor");
// const Account = require('../models/Account');
// const router = express.Router();
const uuidv1 = require("uuid/v1");
const sequelize = require("sequelize");

module.exports = {
  getAllVisitor: async url => {
    // console.log("account Object: " + account.store_name);
    var res = await Visitor.findAll({
      where: {
        account_id: [
          sequelize.literal(
            `(select id from account where store_domain = '` + url + `')`
          )
        ]
      }
    });
    return res;
  },

  addVisitor: async visitor => {
    try {
      var newVisitor = new Visitor(visitor);
      newVisitor.id = uuidv1();
      await newVisitor.save().then(() => {
        console.log(newVisitor);
      });
    } catch (err) {
      console.log(err.message);
    }
  },

  updateVisitor: async visitor => {
    await Visitor.update(visitor, { where: { id: visitor.id } });
  },

  deleteVisitor: async idInput => {
    await Visitor.update(
      {
        status: "false"
      },
      {
        where: {
          id: idInput
        }
      }
    ).then(() => {
      console.log("Removed successfully");
    });
  }
};
