const Account = require("../models/Account");
const uuidv1 = require("uuid/v1");
var CryptoJS = require("crypto-js");

module.exports = {
  createAccount: async acc => {
    try {
      var newAccount = new Account(acc);
      newAccount.id = uuidv1();
      var passwordEncrypt = CryptoJS.SHA256(newAccount.password);
      newAccount.password = passwordEncrypt.toString();
      await newAccount.save().then(() => {
        console.log(newAccount);
      });
    } catch (err) {
      console.log(err);
    }
  }

  // updateTag: async (tag) => {
  //     await tag.update(tag, { where: { id: tag.id } });
  // },

  // deleteTag: async (idtag) => {
  //     console.log("id tag: " + idtag);
  //     await Tag.update({
  //         status: "false"
  //     },
  //         {
  //             where: {
  //                 id: idtag
  //             }
  //         }).then(() => {
  //             console.log("Removed successfully")
  //         });
  //     // res.json({ message: 'Tag removed' })
  // }
};
