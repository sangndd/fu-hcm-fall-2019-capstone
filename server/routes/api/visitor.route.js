const express = require("express");
const router = express.Router();
const db = require("../../config/db");
const Visitor = require("../../models/Visitor");
const visitor_dao = require("../../dao/visitor.dao");
const auth = require("../../middleware/auth.middleware");
//  /**
//  * @swagger
//   * /api/visitor:
//  *   get:
//  *     tags:
//  *       - visitors
//  *     description: Returns all visitors
//  *     produces:
//  *       - application/json
//  *     responses:
//  *       200:
//  *         description: An array of visitors
//  *         schema:
//  *           $ref: '#/definitions/Puppy'
//  */
router.get("/:url", auth, async (req, res) => {
  var url = req.params.url;
  console.log("url: " + url);
  try {
    const visitor = await visitor_dao.getAllVisitor(url);
    res.json(visitor);
  } catch (err) {
    console.log(err.message);
    res.status(500).send("Server error" + err.message);
  }
});

router.post("/", auth, async (req, res) => {
  var visitor = req.body;
  // var email = req.body.email;
  try {
    visitor_dao.addVisitor(visitor);
    res.status(200).json(visitor);
  } catch (err) {
    console.log(err.message);
    res.status(500).send("Server error " + visitor);
  }
});

router.put("/", auth, async (req, res) => {
  var visitor = req.body;
  // var email = req.body.email;
  try {
    visitor_dao.updateVisitor(visitor);
    res.status(200).json(visitor);
  } catch (err) {
    console.log(err.message);
    res.status(500).send("Server error " + visitor);
  }
});

router.delete("/:id", auth, async (req, res) => {
  var id = req.params.id;
  // var email = req.body.email;
  console.log(id);
  try {
    visitor_dao.deleteVisitor(id);
    res.status(200).json("Visitor has been deleted");
  } catch (err) {
    console.log(err.message);
    res.status(500).send("Server error " + visitor);
  }
});

module.exports = router;
