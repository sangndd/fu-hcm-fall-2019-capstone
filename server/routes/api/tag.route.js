const express = require("express");
const router = express.Router();
const db = require("../../config/db");
const tag = require("../../models/Tag");
const tag_dao = require("../../dao/Tag.dao");
const auth = require("../../middleware/auth.middleware");
//  /**
//  * @swagger
//   * /api/Tag:
//  *   get:
//  *     tags:
//  *       - Tags
//  *     description: Returns all Tags
//  *     produces:
//  *       - application/json
//  *     responses:
//  *       200:
//  *         description: An array of Tags
//  *         schema:
//  *           $ref: '#/definitions/Puppy'
//  */
router.get("/", auth, async (req, res) => {
  var url = req.params.url;
  try {
    const tag = await tag_dao.getAllTag();
    res.json(tag);
  } catch (err) {
    console.log(err.message);
    res.status(500).send("Server error" + err.message);
  }
});

router.post("/", auth, async (req, res) => {
  var tag = req.body;
  // var email = req.body.email;
  try {
    tag_dao.addTag(tag);
    res.status(200).json(tag);
  } catch (err) {
    console.log(err.message);
    res.status(500).send("Server error " + tag);
  }
});

router.put("/", auth, async (req, res) => {
  var tag = req.body;
  // var email = req.body.email;
  try {
    tag_dao.updateTag(tag);
    res.status(200).json(tag);
  } catch (err) {
    console.log(err.message);
    res.status(500).send("Server error " + tag);
  }
});

router.delete("/:id", auth, async (req, res) => {
  var id = req.params.id;
  // var email = req.body.email;
  console.log(id);
  try {
    tag_dao.deleteTag(id);
    res.status(200).json("Tag has been deleted");
  } catch (err) {
    console.log(err.message);
    res.status(500).send("Server error " + tag);
  }
});

module.exports = router;
