const express = require("express");
const router = express.Router();
const Account = require("../../models/Account");
const account_dao = require("../../dao/account.dao");
const auth = require("../../middleware/auth.middleware");
router.get("/", auth, async (req, res) => {
  try {
    const account = await Account.findAll({
      attributes: {
        exclude: ["password"]
      }
    });
    res.json(account);
  } catch (err) {
    console.log(err.message);
    res.status(500).send("Server error");
  }
});

router.post("/", auth, async (req, res) => {
  const acc = req.body;
  try {
    console.log("con me may");
    const account = await account_dao.createAccount(acc);
    res.send("Create account successfully");
  } catch (error) {
    console.log(err.message + " at create account");
    res.status(500).send("Server error at creating an account");
  }
});

module.exports = router;
