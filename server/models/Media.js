const Sequelize = require("sequelize");
const db = require("../config/db");

const Media = db.define(
  "media",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    link: {
      type: Sequelize.STRING
    },
    owner_id: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Media;
