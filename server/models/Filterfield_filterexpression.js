const Sequelize = require("sequelize");
const db = require("../config/db");

const Filterfield_filterexpression = db.define(
  "filterfield_filterexpression",
  {
    expression_id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    field_id: {
      type: Sequelize.STRING,
      primaryKey: true
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Filterfield_filterexpression;
