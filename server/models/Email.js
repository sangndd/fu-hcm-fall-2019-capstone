const Sequelize = require("sequelize");
const db = require("../config/db");

const Email = db.define(
  "email",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    from: {
      type: Sequelize.STRING
    },
    subject: {
      type: Sequelize.STRING
    },
    template_id: {
      type: Sequelize.INTEGER
    },
    body: {
      type: Sequelize.STRING
    },
    footer: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Email;
