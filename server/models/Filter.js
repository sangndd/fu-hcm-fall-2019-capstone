const Sequelize = require("sequelize");
const db = require("../config/db");

const Filter = db.define(
  "filter",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    value: {
      type: Sequelize.STRING
    },
    filter_field_id: {
      type: Sequelize.STRING
    },
    filter_expression_id: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Filter;
