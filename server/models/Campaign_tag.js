const Sequelize = require("sequelize");
const db = require("../config/db");

const Campaign_tag = db.define(
  "campaign_tag",
  {
    campaign_id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    tag_id: {
      type: Sequelize.STRING,
      primaryKey: true
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Campaign_tag;
