const Sequelize = require("sequelize");
const db = require("../config/db");

const Campaign = db.define(
  "campaign",
  {
    // paranoid: true,
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    },

    start_date: {
      type: Sequelize.DATE
    },

    end_date: {
      type: Sequelize.DATE
    },
    email_cadence: {
      type: Sequelize.STRING
    },
    status_id: {
      type: Sequelize.STRING
    },

    owner_id: {
      type: Sequelize.STRING
    },

    email_id: {
      type: Sequelize.STRING
    },
    last_modifield_date: {
      type: Sequelize.DATE
    }
  },

  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Campaign;
