const Sequelize = require("sequelize");
const db = require("../config/db");

const Tag = db.define(
  "tag",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    },
    owner_id: {
      type: Sequelize.STRING
    },
    status: {
      type: Sequelize.BOOLEAN
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);
module.exports = Tag;
