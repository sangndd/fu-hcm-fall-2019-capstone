const Sequelize = require("sequelize");
const db = require("../config/db");

const Audienceactivationstatus = db.define(
  "audienceactivationstatus",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Audienceactivationstatus;
