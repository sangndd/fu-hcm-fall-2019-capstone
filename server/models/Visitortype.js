const Sequelize = require("sequelize");
const db = require("../config/db");

const Visitortype = db.define(
  "visitortype",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Visitortype;
