const Sequelize = require("sequelize");
const db = require("../config/db");

const Audience_tag = db.define(
  "audience_tag",
  {
    audience_id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    tag_id: {
      type: Sequelize.STRING,
      primaryKey: true
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Audience_tag;
