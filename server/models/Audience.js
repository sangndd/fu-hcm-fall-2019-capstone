const Sequelize = require("sequelize");
const db = require("../config/db");

const Audience = db.define(
  "audience",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    },
    description: {
      type: Sequelize.STRING
    },
    last_modified_date: {
      type: Sequelize.DATE
    },
    status_id: {
      type: Sequelize.STRING
    },
    criterion_id: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Audience;
