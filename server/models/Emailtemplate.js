const Sequelize = require("sequelize");
const db = require("../config/db");

const Emailtemplate = db.define(
  "emailtemplate",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    },
    body: {
      type: Sequelize.STRING
    },
    footer: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Emailtemplate;
