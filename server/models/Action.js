const Sequelize = require("sequelize");
const db = require("../config/db");

const Action = db.define(
  "action",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    delayed_time: {
      type: Sequelize.INTEGER
    },
    has_follower: {
      type: Sequelize.INTEGER
    },
    follower_id: {
      type: Sequelize.STRING
    },
    email_id: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Action;
