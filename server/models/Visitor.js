const Sequelize = require("sequelize");
const db = require("../config/db");

const Visitor = db.define(
  "visitor",
  {
    // paranoid: true,
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    email: {
      type: Sequelize.STRING
    },
    first_name: {
      type: Sequelize.STRING
    },
    last_name: {
      type: Sequelize.STRING
    },
    last_modified_date: {
      type: Sequelize.DATE
    },
    type_id: {
      type: Sequelize.INTEGER
    },
    account_id: {
      type: Sequelize.INTEGER
    },
    address: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Visitor;
