const Sequelize = require("sequelize");
const db = require("../config/db");

const Campaignoperationstatus = db.define(
  "campaignoperationstatus",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Campaignoperationstatus;
