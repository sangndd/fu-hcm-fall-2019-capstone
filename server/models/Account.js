const Sequelize = require("sequelize");
const db = require("../config/db");

const Account = db.define("account", {
    id: {
        type: Sequelize.STRING,
        primaryKey: true,
    },
    email: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    },
    last_modified_date: {
        type: Sequelize.DATE
    },
    store_domain: {
        type: Sequelize.STRING
    },
    role_id: {
        type: Sequelize.STRING
    },
    status_id: {
        type: Sequelize.STRING
    }
},
{
    timestamps: false,
    freezeTableName: true
});

module.exports = Account;
