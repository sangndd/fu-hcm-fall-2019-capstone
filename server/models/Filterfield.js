const Sequelize = require("sequelize");
const db = require("../config/db");

const Filterfield = db.define(
  "filterfield",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Filterfield;
