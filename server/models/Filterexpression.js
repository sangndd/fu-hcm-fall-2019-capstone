const Sequelize = require("sequelize");
const db = require("../config/db");

const Filterexpression = db.define(
  "filterexpression",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    value: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Filterexpression;
