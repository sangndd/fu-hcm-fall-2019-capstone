const Sequelize = require("sequelize");
const db = require("../config/db");

const Criteriontype = db.define(
  "criteriontype",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Criteriontype;
