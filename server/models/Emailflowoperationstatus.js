const Sequelize = require("sequelize");
const db = require("../config/db");

const Emailflowoperationstatus = db.define(
  "emailflowoperationstatus",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Emailflowoperationstatus;
