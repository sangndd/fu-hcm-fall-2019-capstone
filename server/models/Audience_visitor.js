const Sequelize = require("sequelize");
const db = require("../config/db");

const Audience_visitor = db.define(
  "audience_visitor",
  {
    audience_id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    visitor_id: {
      type: Sequelize.STRING,
      primaryKey: true
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Audience_visitor;
