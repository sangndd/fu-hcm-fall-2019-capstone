const Sequelize = require("sequelize");
const db = require("../config/db");

const Filterfield_criteriontype = db.define(
  "filterfield_criteriontype",
  {
    criterion_type_id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    filter_field_id: {
      type: Sequelize.STRING,
      primaryKey: true
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Filterfield_criteriontype;
