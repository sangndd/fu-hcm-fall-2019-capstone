const Sequelize = require("sequelize");
const db = require("../config/db");

const Criterionfollowerexpression = db.define(
  "criterionfollowerexpression",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    value: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Criterionfollowerexpression;
