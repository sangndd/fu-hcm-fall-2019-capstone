const Sequelize = require("sequelize");
const db = require("../config/db");

const Accountactivationstatus = db.define(
  "accountactivationstatus",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Accountactivationstatus;
