const Sequelize = require("sequelize");
const db = require("../config/db");

const Emailflowtrigger = db.define(
  "emailflowtrigger",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Emailflowtrigger;
