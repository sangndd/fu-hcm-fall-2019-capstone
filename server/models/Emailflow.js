const Sequelize = require("sequelize");
const db = require("../config/db");

const Emailflow = db.define(
  "emailflow",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    },
    last_modified_date: {
      type: Sequelize.DATE
    },
    owner_id: {
      type: Sequelize.STRING
    },
    status_id: {
      type: Sequelize.STRING
    },
    trigger_id: {
      type: Sequelize.STRING
    },
    action_id: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Emailflow;
