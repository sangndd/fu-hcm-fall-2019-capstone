const Sequelize = require("sequelize");
const db = require("../config/db");

const Criterion = db.define(
  "criterion",
  {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    type_id: {
      type: Sequelize.STRING
    },
    has_follower: {
      type: Sequelize.INTEGER
    },
    filter_id: {
      type: Sequelize.STRING
    },
    follower_id: {
      type: Sequelize.STRING
    },
    follower_expression_id: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Criterion;
