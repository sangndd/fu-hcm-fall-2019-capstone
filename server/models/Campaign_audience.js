const Sequelize = require("sequelize");
const db = require("../config/db");

const Campaign_audience = db.define(
  "campaign_audience",
  {
    audience_id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    campaign_id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    has_opened_email: {
      type: Sequelize.INTEGER
    }
  },
  {
    timestamps: false,
    freezeTableName: true
  }
);

module.exports = Campaign_audience;
