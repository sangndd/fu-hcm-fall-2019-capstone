CREATE DATABASE  IF NOT EXISTS `EMM` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `EMM`;
-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 198.71.225.61    Database: EMM
-- ------------------------------------------------------
-- Server version	5.5.51-38.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Account`
--

DROP TABLE IF EXISTS `Account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Account` (
  `id` char(36) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `last_modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `store_domain` varchar(255) DEFAULT NULL,
  `store_name` varchar(100) DEFAULT NULL,
  `role_id` char(36) NOT NULL,
  `status_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `FK_Account_has_Role` (`role_id`),
  KEY `FK_Account_has_Status` (`status_id`),
  CONSTRAINT `FK_Account_has_Role` FOREIGN KEY (`role_id`) REFERENCES `Role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Account_has_Status` FOREIGN KEY (`status_id`) REFERENCES `AccountActivationStatus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Account`
--
-- ORDER BY:  `id`

LOCK TABLES `Account` WRITE;
/*!40000 ALTER TABLE `Account` DISABLE KEYS */;
INSERT INTO `Account` (`id`, `email`, `password`, `last_modified_date`, `store_domain`, `store_name`, `role_id`, `status_id`) VALUES ('4e88a811-3ac3-4e9b-a98a-9e5364011059','admin@emm.com','5f96f0b23589762e62f80f8797b9b99caabcd9daa7f06e83b5f6532d55a643f3wZAzaTkVSDnWN0GGIzS7Ww==','2019-09-27 12:29:42',NULL,NULL,'d02e63f4-dc98-42b7-ac1a-6c01229510df','ecb017ee-8cd4-49d3-b73b-424bab2c6422');
/*!40000 ALTER TABLE `Account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AccountActivationStatus`
--

DROP TABLE IF EXISTS `AccountActivationStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AccountActivationStatus` (
  `id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AccountActivationStatus`
--
-- ORDER BY:  `id`

LOCK TABLES `AccountActivationStatus` WRITE;
/*!40000 ALTER TABLE `AccountActivationStatus` DISABLE KEYS */;
INSERT INTO `AccountActivationStatus` (`id`, `name`) VALUES ('a43113d0-644e-4122-a81f-14759e5407d8','Deactivated'),('ecb017ee-8cd4-49d3-b73b-424bab2c6422','Activated');
/*!40000 ALTER TABLE `AccountActivationStatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Action`
--

DROP TABLE IF EXISTS `Action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Action` (
  `id` char(36) NOT NULL,
  `delayed_time` int(11) NOT NULL DEFAULT '0',
  `has_follower` tinyint(1) NOT NULL DEFAULT '0',
  `follower_id` char(36) DEFAULT NULL,
  `email_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Action_has_Follower` (`follower_id`),
  KEY `FK_Aciton_contains_Email` (`email_id`),
  CONSTRAINT `FK_Action_has_Follower` FOREIGN KEY (`follower_id`) REFERENCES `Action` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Aciton_contains_Email` FOREIGN KEY (`email_id`) REFERENCES `Email` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Action`
--
-- ORDER BY:  `id`

LOCK TABLES `Action` WRITE;
/*!40000 ALTER TABLE `Action` DISABLE KEYS */;
/*!40000 ALTER TABLE `Action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Audience`
--

DROP TABLE IF EXISTS `Audience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Audience` (
  `id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `last_modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` char(36) NOT NULL,
  `criterion_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Audience_has_Status` (`status_id`),
  KEY `FK_Audience_has_Criterion` (`criterion_id`),
  CONSTRAINT `FK_Audience_has_Criterion` FOREIGN KEY (`criterion_id`) REFERENCES `Criterion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Audience_has_Status` FOREIGN KEY (`status_id`) REFERENCES `AudienceActivationStatus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Audience`
--
-- ORDER BY:  `id`

LOCK TABLES `Audience` WRITE;
/*!40000 ALTER TABLE `Audience` DISABLE KEYS */;
/*!40000 ALTER TABLE `Audience` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AudienceActivationStatus`
--

DROP TABLE IF EXISTS `AudienceActivationStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AudienceActivationStatus` (
  `id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AudienceActivationStatus`
--
-- ORDER BY:  `id`

LOCK TABLES `AudienceActivationStatus` WRITE;
/*!40000 ALTER TABLE `AudienceActivationStatus` DISABLE KEYS */;
INSERT INTO `AudienceActivationStatus` (`id`, `name`) VALUES ('573600b1-a19e-48ed-ba62-cea9ddaaa3df','Activated'),('dd4c512d-483a-4a66-aa34-b074080541fe','Deactivated');
/*!40000 ALTER TABLE `AudienceActivationStatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Audience_Tag`
--

DROP TABLE IF EXISTS `Audience_Tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Audience_Tag` (
  `audience_id` char(36) NOT NULL,
  `tag_id` char(36) NOT NULL,
  PRIMARY KEY (`audience_id`,`tag_id`),
  KEY `FK_Audience_Tag_TagID` (`tag_id`),
  KEY `FK_Audience_Tag_AudienceID` (`audience_id`),
  CONSTRAINT `FK_Audience_Tag_AudienceID` FOREIGN KEY (`audience_id`) REFERENCES `Audience` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Audience_Tag_TagID` FOREIGN KEY (`tag_id`) REFERENCES `Tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Audience_Tag`
--
-- ORDER BY:  `audience_id`,`tag_id`

LOCK TABLES `Audience_Tag` WRITE;
/*!40000 ALTER TABLE `Audience_Tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `Audience_Tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Audience_Visitor`
--

DROP TABLE IF EXISTS `Audience_Visitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Audience_Visitor` (
  `audience_id` char(36) NOT NULL,
  `visitor_id` char(36) NOT NULL,
  PRIMARY KEY (`audience_id`,`visitor_id`),
  KEY `FK_Audience_Visitor_VisitorID` (`visitor_id`),
  KEY `FK_Audience_VIsitor_AudienceID` (`audience_id`),
  CONSTRAINT `FK_Audience_Visitor_AudienceID` FOREIGN KEY (`audience_id`) REFERENCES `Audience` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Audience_Visitor_VisitorID` FOREIGN KEY (`visitor_id`) REFERENCES `Visitor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Audience_Visitor`
--
-- ORDER BY:  `audience_id`,`visitor_id`

LOCK TABLES `Audience_Visitor` WRITE;
/*!40000 ALTER TABLE `Audience_Visitor` DISABLE KEYS */;
/*!40000 ALTER TABLE `Audience_Visitor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Campaign`
--

DROP TABLE IF EXISTS `Campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Campaign` (
  `id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `email_cadence` varchar(25) NOT NULL DEFAULT "7",
  `last_modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` char(36) NOT NULL,
  `owner_id` char(36) NOT NULL,
  `email_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Campaign_has_Owner` (`owner_id`),
  KEY `FK_Campaign_has_Status` (`status_id`),
  KEY `FK_Campaign_has_Email` (`email_id`),
  CONSTRAINT `FK_Campaign_has_Email` FOREIGN KEY (`email_id`) REFERENCES `Email` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Campaign_has_Owner` FOREIGN KEY (`owner_id`) REFERENCES `Account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Campaign_has_Status` FOREIGN KEY (`status_id`) REFERENCES `CampaignOperationStatus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Campaign`
--
-- ORDER BY:  `id`

LOCK TABLES `Campaign` WRITE;
/*!40000 ALTER TABLE `Campaign` DISABLE KEYS */;
/*!40000 ALTER TABLE `Campaign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CampaignOperationStatus`
--

DROP TABLE IF EXISTS `CampaignOperationStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CampaignOperationStatus` (
  `id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CampaignOperationStatus`
--
-- ORDER BY:  `id`

LOCK TABLES `CampaignOperationStatus` WRITE;
/*!40000 ALTER TABLE `CampaignOperationStatus` DISABLE KEYS */;
INSERT INTO `CampaignOperationStatus` (`id`, `name`) VALUES ('4cece002-1ae6-4bad-9633-ab30dc55b4e5','Running'),('a6ee5994-1e42-467e-8cf7-e9132f08e292','Draft'),('c0316ed0-5fb4-491c-a581-62a251b1be8d','Completed');
/*!40000 ALTER TABLE `CampaignOperationStatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Campaign_Audience`
--

DROP TABLE IF EXISTS `Campaign_Audience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Campaign_Audience` (
  `audience_id` char(36) NOT NULL,
  `campaign_id` char(36) NOT NULL,
  `has_opened_email` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`audience_id`,`campaign_id`),
  KEY `FK_Campaign_Audience_CampaignID` (`campaign_id`),
  KEY `FK_Campaign_Audience_AudienceID` (`audience_id`),
  CONSTRAINT `FK_Campaign_Audience_AudienceID` FOREIGN KEY (`audience_id`) REFERENCES `Audience` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Campaign_Audience_CampaignID` FOREIGN KEY (`campaign_id`) REFERENCES `Campaign` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Campaign_Audience`
--
-- ORDER BY:  `audience_id`,`campaign_id`

LOCK TABLES `Campaign_Audience` WRITE;
/*!40000 ALTER TABLE `Campaign_Audience` DISABLE KEYS */;
/*!40000 ALTER TABLE `Campaign_Audience` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Campaign_Tag`
--

DROP TABLE IF EXISTS `Campaign_Tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Campaign_Tag` (
  `campaign_id` char(36) NOT NULL,
  `tag_id` char(36) NOT NULL,
  PRIMARY KEY (`campaign_id`,`tag_id`),
  KEY `FK_Campaign_Tag_TagID` (`tag_id`),
  KEY `FK_Campaign_Tag_CampaignID` (`campaign_id`),
  CONSTRAINT `FK_Campaign_Tag_CampaignID` FOREIGN KEY (`campaign_id`) REFERENCES `Campaign` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Campaign_Tag_TagID` FOREIGN KEY (`tag_id`) REFERENCES `Tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Campaign_Tag`
--
-- ORDER BY:  `campaign_id`,`tag_id`

LOCK TABLES `Campaign_Tag` WRITE;
/*!40000 ALTER TABLE `Campaign_Tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `Campaign_Tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Criterion`
--

DROP TABLE IF EXISTS `Criterion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Criterion` (
  `id` char(36) NOT NULL,
  `type_id` char(36) NOT NULL,
  `filter_id` char(36) NOT NULL,
  `has_follower` tinyint(1) NOT NULL DEFAULT '0',
  `follower_id` char(36) DEFAULT NULL,
  `follower_expression_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Criterion_has_Type` (`type_id`),
  KEY `FK_Criterion_has_Filter` (`filter_id`),
  KEY `FK_Criterion_has_Follower` (`follower_id`),
  KEY `FK_Criterion_Follower_has_Expression` (`follower_expression_id`),
  CONSTRAINT `FK_Criterion_Follower_has_Expression` FOREIGN KEY (`follower_expression_id`) REFERENCES `CriterionFollowerExpression` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Criterion_has_Filter` FOREIGN KEY (`filter_id`) REFERENCES `Filter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Criterion_has_Follower` FOREIGN KEY (`follower_id`) REFERENCES `Criterion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Criterion_has_Type` FOREIGN KEY (`type_id`) REFERENCES `CriterionType` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Criterion`
--
-- ORDER BY:  `id`

LOCK TABLES `Criterion` WRITE;
/*!40000 ALTER TABLE `Criterion` DISABLE KEYS */;
/*!40000 ALTER TABLE `Criterion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CriterionFollowerExpression`
--

DROP TABLE IF EXISTS `CriterionFollowerExpression`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CriterionFollowerExpression` (
  `id` char(36) NOT NULL,
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `value` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CriterionFollowerExpression`
--
-- ORDER BY:  `id`

LOCK TABLES `CriterionFollowerExpression` WRITE;
/*!40000 ALTER TABLE `CriterionFollowerExpression` DISABLE KEYS */;
INSERT INTO `CriterionFollowerExpression` (`id`, `value`) VALUES ('81ddd0e0-7b6a-49f1-bb9a-9457aab09f0c','And'),('acaf4056-cee0-4cdb-ada1-360b07f3c275','Or');
/*!40000 ALTER TABLE `CriterionFollowerExpression` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CriterionType`
--

DROP TABLE IF EXISTS `CriterionType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CriterionType` (
  `id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CriterionType`
--
-- ORDER BY:  `id`

LOCK TABLES `CriterionType` WRITE;
/*!40000 ALTER TABLE `CriterionType` DISABLE KEYS */;
INSERT INTO `CriterionType` (`id`, `name`) VALUES ('fee67a7c-cbf5-4deb-abd3-33add96c35f7','E-Commerce');
/*!40000 ALTER TABLE `CriterionType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Email`
--

DROP TABLE IF EXISTS `Email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Email` (
  `id` char(36) NOT NULL,
  `from` varchar(100) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `template_id` char(36) NOT NULL,
  `body` text NOT NULL,
  `footer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Email_has_Template` (`template_id`),
  CONSTRAINT `FK_Email_has_Template` FOREIGN KEY (`template_id`) REFERENCES `EmailTemplate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Email`
--
-- ORDER BY:  `id`

LOCK TABLES `Email` WRITE;
/*!40000 ALTER TABLE `Email` DISABLE KEYS */;
/*!40000 ALTER TABLE `Email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EmailFlow`
--

DROP TABLE IF EXISTS `EmailFlow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EmailFlow` (
  `id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  `last_modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `owner_id` char(36) NOT NULL,
  `status_id` char(36) NOT NULL,
  `trigger_id` char(36) NOT NULL,
  `action_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_EmailFlow_has_Trigger` (`trigger_id`),
  KEY `FK_EmailFlow_has_Action` (`action_id`),
  KEY `FK_EmailFlow_belong_to_Owner_idx` (`owner_id`),
  KEY `FK_EmailFlow_has_Status_idx` (`status_id`),
  CONSTRAINT `FK_EmailFlow_has_Status` FOREIGN KEY (`status_id`) REFERENCES `EmailFlowOperationStatus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_EmailFlow_belong_to_Owner` FOREIGN KEY (`owner_id`) REFERENCES `Account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_EmailFlow_has_Action` FOREIGN KEY (`action_id`) REFERENCES `Action` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_EmailFlow_has_Trigger` FOREIGN KEY (`trigger_id`) REFERENCES `EmailFlowTrigger` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EmailFlow`
--
-- ORDER BY:  `id`

LOCK TABLES `EmailFlow` WRITE;
/*!40000 ALTER TABLE `EmailFlow` DISABLE KEYS */;
/*!40000 ALTER TABLE `EmailFlow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EmailFlowOperationStatus`
--

DROP TABLE IF EXISTS `EmailFlowOperationStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EmailFlowOperationStatus` (
  `id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EmailFlowOperationStatus`
--
-- ORDER BY:  `id`

LOCK TABLES `EmailFlowOperationStatus` WRITE;
/*!40000 ALTER TABLE `EmailFlowOperationStatus` DISABLE KEYS */;
INSERT INTO `EmailFlowOperationStatus` (`id`, `name`) VALUES ('a960e1ac-d8bc-4e8f-855d-4206458b8030','Running'),('dfcd7682-b63c-4444-9890-44abe0fc8c30','Stopped');
/*!40000 ALTER TABLE `EmailFlowOperationStatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EmailFlowTrigger`
--

DROP TABLE IF EXISTS `EmailFlowTrigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EmailFlowTrigger` (
  `id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EmailFlowTrigger`
--
-- ORDER BY:  `id`

LOCK TABLES `EmailFlowTrigger` WRITE;
/*!40000 ALTER TABLE `EmailFlowTrigger` DISABLE KEYS */;
INSERT INTO `EmailFlowTrigger` (`id`, `name`) VALUES ('538b13e6-945e-468e-a0c1-f4463ae339e0','FulfillOrder'),('70730fd9-16e6-4db7-a8f3-bb69e6b0d479','JoinAudience'),('cc0184f6-f0a3-4b95-9dc3-9dce9ccf5b9d','AbandonedCheckout');
/*!40000 ALTER TABLE `EmailFlowTrigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EmailTemplate`
--

DROP TABLE IF EXISTS `EmailTemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EmailTemplate` (
  `id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  `body` text NOT NULL,
  `footer` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EmailTemplate`
--
-- ORDER BY:  `id`

LOCK TABLES `EmailTemplate` WRITE;
/*!40000 ALTER TABLE `EmailTemplate` DISABLE KEYS */;
/*!40000 ALTER TABLE `EmailTemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Filter`
--

DROP TABLE IF EXISTS `Filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Filter` (
  `id` char(36) NOT NULL,
  `value` varchar(255) NOT NULL,
  `filter_field_id` char(36) NOT NULL,
  `filter_expression_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Filter_has_Field` (`filter_field_id`),
  KEY `FK_Filter_has_Expression` (`filter_expression_id`),
  CONSTRAINT `FK_Filter_has_Field` FOREIGN KEY (`filter_field_id`) REFERENCES `FilterField` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Filter_has_Expression` FOREIGN KEY (`filter_expression_id`) REFERENCES `FilterExpression` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Filter`
--
-- ORDER BY:  `id`

LOCK TABLES `Filter` WRITE;
/*!40000 ALTER TABLE `Filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `Filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FilterExpression`
--

DROP TABLE IF EXISTS `FilterExpression`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FilterExpression` (
  `id` char(36) NOT NULL,
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `value` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FilterExpression`
--
-- ORDER BY:  `id`

LOCK TABLES `FilterExpression` WRITE;
/*!40000 ALTER TABLE `FilterExpression` DISABLE KEYS */;
INSERT INTO `FilterExpression` (`id`, `value`) VALUES ('26634679-3a78-4b49-b4d0-08316f1467ea','after'),('3b7e2232-3a44-4564-bb3a-e2c3d9c2a8ec','greater than'),('3d0b668d-34d2-463a-9663-8517621d401b','before'),('4a2649da-8246-434a-b536-8a86887a64ad','less than'),('6d7dfd7f-119d-4bf2-ba63-cf64ecc34928','ends with'),('afb24f5a-6ac9-4bfe-b188-f8c06386ca75','between'),('b2fe175b-2598-43da-9999-96fb0ce82aca','equals'),('d3322e4e-5416-4237-90ec-a8aa23197ae6','starts with'),('f3dc16d5-ed3a-4ecb-9ff4-fc8dade2ad7a','contains');
/*!40000 ALTER TABLE `FilterExpression` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FilterField`
--

DROP TABLE IF EXISTS `FilterField`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FilterField` (
  `id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FilterField`
--
-- ORDER BY:  `id`

LOCK TABLES `FilterField` WRITE;
/*!40000 ALTER TABLE `FilterField` DISABLE KEYS */;
INSERT INTO `FilterField` (`id`, `name`) VALUES ('3508a904-ee3d-4823-b392-7c04a1085294','TotalOrder'),('3b6261cd-b8cf-4df5-81ed-667f022c1a97','ProductPurchased'),('4eeb46e3-50dc-4f05-8338-8892b6dc14ca','OrderedProductValue'),('676c95b0-7780-4085-b56f-a312fc57cd6d','CanceledOrderTimes'),('6cb5fc2e-6c8c-4174-b96e-5a157591e477','PurchasedDate'),('d70af9ab-8b97-4950-b6e3-3daf77816a1f','FulfilledOrderTimes'),('f174714c-2bba-46c1-a5d5-55e99f2f5583','TotalSpent');
/*!40000 ALTER TABLE `FilterField` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FilterField_CriterionType`
--

DROP TABLE IF EXISTS `FilterField_CriterionType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FilterField_CriterionType` (
  `criterion_type_id` char(36) NOT NULL,
  `filter_field_id` char(36) NOT NULL,
  PRIMARY KEY (`criterion_type_id`,`filter_field_id`),
  KEY `FK_FilterField_CriterionType_FilterFieldID` (`filter_field_id`),
  KEY `FK_FilterField_CriterionType_CriterionTypeID` (`criterion_type_id`),
  CONSTRAINT `FK_FilterField_CriterionType_CriterionTypeID` FOREIGN KEY (`criterion_type_id`) REFERENCES `CriterionType` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FilterField_CriterionType_FilterFieldID` FOREIGN KEY (`filter_field_id`) REFERENCES `FilterField` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FilterField_CriterionType`
--
-- ORDER BY:  `criterion_type_id`,`filter_field_id`

LOCK TABLES `FilterField_CriterionType` WRITE;
/*!40000 ALTER TABLE `FilterField_CriterionType` DISABLE KEYS */;
INSERT INTO `FilterField_CriterionType` (`criterion_type_id`, `filter_field_id`) VALUES ('fee67a7c-cbf5-4deb-abd3-33add96c35f7','3508a904-ee3d-4823-b392-7c04a1085294'),('fee67a7c-cbf5-4deb-abd3-33add96c35f7','3b6261cd-b8cf-4df5-81ed-667f022c1a97'),('fee67a7c-cbf5-4deb-abd3-33add96c35f7','4eeb46e3-50dc-4f05-8338-8892b6dc14ca'),('fee67a7c-cbf5-4deb-abd3-33add96c35f7','676c95b0-7780-4085-b56f-a312fc57cd6d'),('fee67a7c-cbf5-4deb-abd3-33add96c35f7','6cb5fc2e-6c8c-4174-b96e-5a157591e477'),('fee67a7c-cbf5-4deb-abd3-33add96c35f7','d70af9ab-8b97-4950-b6e3-3daf77816a1f'),('fee67a7c-cbf5-4deb-abd3-33add96c35f7','f174714c-2bba-46c1-a5d5-55e99f2f5583');
/*!40000 ALTER TABLE `FilterField_CriterionType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FilterField_FilterExpression`
--

DROP TABLE IF EXISTS `FilterField_FilterExpression`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FilterField_FilterExpression` (
  `expression_id` char(36) NOT NULL,
  `field_id` char(36) NOT NULL,
  PRIMARY KEY (`expression_id`,`field_id`),
  KEY `FK_FilterField_FilterExpression_FilterFieldID` (`field_id`),
  KEY `FK_FiltterField_FilterExpression_FilterExpressionID` (`expression_id`),
  CONSTRAINT `FK_FilterField_FilterExpression_FilterExpressionID` FOREIGN KEY (`expression_id`) REFERENCES `FilterExpression` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FilterField_FilterExpression_FilterFieldID` FOREIGN KEY (`field_id`) REFERENCES `FilterField` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FilterField_FilterExpression`
--
-- ORDER BY:  `expression_id`,`field_id`

LOCK TABLES `FilterField_FilterExpression` WRITE;
/*!40000 ALTER TABLE `FilterField_FilterExpression` DISABLE KEYS */;
INSERT INTO `FilterField_FilterExpression` (`expression_id`, `field_id`) VALUES ('26634679-3a78-4b49-b4d0-08316f1467ea','6cb5fc2e-6c8c-4174-b96e-5a157591e477'),('3b7e2232-3a44-4564-bb3a-e2c3d9c2a8ec','3508a904-ee3d-4823-b392-7c04a1085294'),('3b7e2232-3a44-4564-bb3a-e2c3d9c2a8ec','4eeb46e3-50dc-4f05-8338-8892b6dc14ca'),('3b7e2232-3a44-4564-bb3a-e2c3d9c2a8ec','676c95b0-7780-4085-b56f-a312fc57cd6d'),('3b7e2232-3a44-4564-bb3a-e2c3d9c2a8ec','d70af9ab-8b97-4950-b6e3-3daf77816a1f'),('3b7e2232-3a44-4564-bb3a-e2c3d9c2a8ec','f174714c-2bba-46c1-a5d5-55e99f2f5583'),('3d0b668d-34d2-463a-9663-8517621d401b','6cb5fc2e-6c8c-4174-b96e-5a157591e477'),('4a2649da-8246-434a-b536-8a86887a64ad','3508a904-ee3d-4823-b392-7c04a1085294'),('4a2649da-8246-434a-b536-8a86887a64ad','4eeb46e3-50dc-4f05-8338-8892b6dc14ca'),('4a2649da-8246-434a-b536-8a86887a64ad','676c95b0-7780-4085-b56f-a312fc57cd6d'),('4a2649da-8246-434a-b536-8a86887a64ad','d70af9ab-8b97-4950-b6e3-3daf77816a1f'),('4a2649da-8246-434a-b536-8a86887a64ad','f174714c-2bba-46c1-a5d5-55e99f2f5583'),('6d7dfd7f-119d-4bf2-ba63-cf64ecc34928','3b6261cd-b8cf-4df5-81ed-667f022c1a97'),('afb24f5a-6ac9-4bfe-b188-f8c06386ca75','3508a904-ee3d-4823-b392-7c04a1085294'),('afb24f5a-6ac9-4bfe-b188-f8c06386ca75','4eeb46e3-50dc-4f05-8338-8892b6dc14ca'),('afb24f5a-6ac9-4bfe-b188-f8c06386ca75','676c95b0-7780-4085-b56f-a312fc57cd6d'),('afb24f5a-6ac9-4bfe-b188-f8c06386ca75','6cb5fc2e-6c8c-4174-b96e-5a157591e477'),('afb24f5a-6ac9-4bfe-b188-f8c06386ca75','d70af9ab-8b97-4950-b6e3-3daf77816a1f'),('afb24f5a-6ac9-4bfe-b188-f8c06386ca75','f174714c-2bba-46c1-a5d5-55e99f2f5583'),('b2fe175b-2598-43da-9999-96fb0ce82aca','3508a904-ee3d-4823-b392-7c04a1085294'),('b2fe175b-2598-43da-9999-96fb0ce82aca','3b6261cd-b8cf-4df5-81ed-667f022c1a97'),('b2fe175b-2598-43da-9999-96fb0ce82aca','4eeb46e3-50dc-4f05-8338-8892b6dc14ca'),('b2fe175b-2598-43da-9999-96fb0ce82aca','676c95b0-7780-4085-b56f-a312fc57cd6d'),('b2fe175b-2598-43da-9999-96fb0ce82aca','6cb5fc2e-6c8c-4174-b96e-5a157591e477'),('b2fe175b-2598-43da-9999-96fb0ce82aca','d70af9ab-8b97-4950-b6e3-3daf77816a1f'),('b2fe175b-2598-43da-9999-96fb0ce82aca','f174714c-2bba-46c1-a5d5-55e99f2f5583'),('d3322e4e-5416-4237-90ec-a8aa23197ae6','3b6261cd-b8cf-4df5-81ed-667f022c1a97'),('f3dc16d5-ed3a-4ecb-9ff4-fc8dade2ad7a','3b6261cd-b8cf-4df5-81ed-667f022c1a97');
/*!40000 ALTER TABLE `FilterField_FilterExpression` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Media`
--

DROP TABLE IF EXISTS `Media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Media` (
  `id` char(36) NOT NULL,
  `link` varchar(255) NOT NULL,
  `owner_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Media_belong_to_Owner` (`owner_id`),
  CONSTRAINT `FK_Media_belong_to_Owner` FOREIGN KEY (`owner_id`) REFERENCES `Account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Media`
--
-- ORDER BY:  `id`

LOCK TABLES `Media` WRITE;
/*!40000 ALTER TABLE `Media` DISABLE KEYS */;
/*!40000 ALTER TABLE `Media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Role`
--

DROP TABLE IF EXISTS `Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Role` (
  `id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Role`
--
-- ORDER BY:  `id`

LOCK TABLES `Role` WRITE;
/*!40000 ALTER TABLE `Role` DISABLE KEYS */;
INSERT INTO `Role` (`id`, `name`) VALUES ('d02e63f4-dc98-42b7-ac1a-6c01229510df','SysAdmin'),('ecb017ee-8cd4-49d3-b73b-424bab2c6422','StoreOwner');
/*!40000 ALTER TABLE `Role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tag`
--

DROP TABLE IF EXISTS `Tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tag` (
  `id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  `owner_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Tag_belong_to_Owner` (`owner_id`),
  CONSTRAINT `FK_Tag_belong_to_Owner` FOREIGN KEY (`owner_id`) REFERENCES `Account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tag`
--
-- ORDER BY:  `id`

LOCK TABLES `Tag` WRITE;
/*!40000 ALTER TABLE `Tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Visitor`
--

DROP TABLE IF EXISTS `Visitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Visitor` (
  `id` char(36) NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `last_modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `type_id` char(36) NOT NULL,
  `account_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `FK_Visitor_belong_to_Account` (`account_id`),
  KEY `FK_Visitor_has_Type` (`type_id`),
  CONSTRAINT `FK_Visitor_has_Type` FOREIGN KEY (`type_id`) REFERENCES `VisitorType` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Visitor_belong_to_Account` FOREIGN KEY (`account_id`) REFERENCES `Account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Visitor`
--
-- ORDER BY:  `id`

LOCK TABLES `Visitor` WRITE;
/*!40000 ALTER TABLE `Visitor` DISABLE KEYS */;
/*!40000 ALTER TABLE `Visitor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VisitorType`
--

DROP TABLE IF EXISTS `VisitorType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `VisitorType` (
  `id` char(36) NOT NULL,
  `name` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VisitorType`
--
-- ORDER BY:  `id`

LOCK TABLES `VisitorType` WRITE;
/*!40000 ALTER TABLE `VisitorType` DISABLE KEYS */;
INSERT INTO `VisitorType` (`id`, `name`) VALUES ('af49580e-5d02-4390-9958-ee00d0f1f6ee','Subscriber'),('fb12ceae-c62a-4e99-aeae-ce2fbe83535c','Customer');
/*!40000 ALTER TABLE `VisitorType` ENABLE KEYS */;
UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-30 12:42:51
