import { all, takeEvery, put, call } from 'redux-saga/effects'
import { getTags } from '../../services/tag'
import actions from './actions'

// eslint-disable-next-line import/prefer-default-export
export function* GET_DATA_TAGS() {
  const tags = yield call(getTags)
  console.log(tags)
  yield put({
    type: 'tag/SET_STATE',
    payload: {
      tags,
    },
  })
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.GET_DATA_TAGS, GET_DATA_TAGS),
    // takeEvery(actions.LOAD_CURRENT_ACCOUNT, LOAD_CURRENT_ACCOUNT),
    // LOAD_CURRENT_ACCOUNT(),
  ])
}
