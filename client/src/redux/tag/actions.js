const actions = {
  SET_STATE: 'tag/SET_STATE',
  GET_DATA_TAGS: 'tag/GET_DATA_TAGS',
  // LOAD_CURRENT_ACCOUNT: 'behavior/LOAD_CURRENT_ACCOUNT',
}

export default actions
