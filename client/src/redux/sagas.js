import { all } from 'redux-saga/effects'
import user from './user/sagas'
import menu from './menu/sagas'
import settings from './settings/sagas'
import visitor from './visitor/sagas'
import tag from './tag/sagas'

export default function* rootSaga() {
  yield all([user(), menu(), settings(), tag(), visitor()])
}
