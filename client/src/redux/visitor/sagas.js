import { all, takeEvery, put, call } from 'redux-saga/effects'
import { getVisitors } from '../../services/visitor'
import actions from './actions'

// eslint-disable-next-line import/prefer-default-export
export function* GET_DATA_VISITORS() {
  const visitors = yield call(getVisitors, 'cuongdola.shopify.com')
  console.log(visitors)
  yield put({
    type: 'visitor/SET_STATE',
    payload: {
      visitors,
    },
  })
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.GET_DATA_VISITORS, GET_DATA_VISITORS),
    // takeEvery(actions.LOAD_CURRENT_ACCOUNT, LOAD_CURRENT_ACCOUNT),
    // LOAD_CURRENT_ACCOUNT(),
  ])
}
