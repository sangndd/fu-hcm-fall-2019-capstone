const actions = {
  SET_STATE: 'visitor/SET_STATE',
  GET_DATA_VISITORS: 'visitor/GET_DATA_VISITORS',
  // LOAD_CURRENT_ACCOUNT: 'behavior/LOAD_CURRENT_ACCOUNT',
}

export default actions
