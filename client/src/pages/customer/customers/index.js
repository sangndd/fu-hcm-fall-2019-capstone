import React from 'react'
import {
  Table,
  Button,
  Dropdown,
  Icon,
  message,
  Menu,
  Tag,
  Modal,
  Select,
  notification,
  Row,
} from 'antd'
import { connect } from 'react-redux'
// import Moment from 'react-moment';
// import moment from 'moment';

const { confirm } = Modal
// const OPTIONS = []
const openNotification = () => {
  notification.open({
    message: 'Added Tags',
    description:
      'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
    icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
  })
}

function handleButtonClick(e) {
  message.info('Click on left button.')
  console.log('click left button', e)
}

// function handleMenuClick(e) {
//   message.info('Click on menu item.')
//   console.log('click', e)
// }

function showConfirm() {
  confirm({
    title: 'Do you Want to delete these items?',
    content: 'Some descriptions',
    onOk() {
      console.log('OK')
    },
    onCancel() {
      console.log('Cancel')
    },
  })
}

const menu = (
  <Menu onClick={showConfirm}>
    <Menu.Item key="1">
      <Icon type="user" />
      Unsubscribered
    </Menu.Item>
    <Menu.Item key="2">
      <Icon type="user" />
      ReSubscribe
    </Menu.Item>
    <Menu.Item key="3">
      <Icon type="user" />
      Vip
    </Menu.Item>
    <Menu.Item key="4">
      <Icon type="user" />
      Remove subscriber
    </Menu.Item>
  </Menu>
)

// const data = []
// for (let i = 0; i < this.props.tag.tags.length; i += 1) {
//   console.log(this.props.tag.tags);
//     OPTIONS.push({
//     key: i.id,
//     name: i.name
//   })
// }

@connect(({ visitor }) => ({ visitor }))
@connect(({ tag }) => ({ tag }))
class Customers extends React.Component {
  constructor(props) {
    super(props)
    this.props = props
  }

  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
    selectedItems: [],
    visible: false,
    // tagInput: [
    //   // {
    //   //   name: 'Email',
    //   //   id: 'id'
    //   // },
    //   // {
    //   //   name: '123',
    //   //   id: '2'
    //   // }
    // ]
  }

  componentDidUpdate = () => {
    const { tag } = this.props
    const { tags } = tag
    // this.tags = tags;
    console.log(tags)
  }

  componentDidMount = () => {
    const { dispatch } = this.props

    dispatch({ type: 'visitor/GET_DATA_VISITORS' })
    dispatch({ type: 'tag/GET_DATA_TAGS' })
  }

  showModal = () => {
    this.setState({
      visible: true,
    })
  }

  handleOk = e => {
    console.log(e)
    this.setState({
      visible: false,
    })
  }

  handleCancel = e => {
    console.log(e)
    this.setState({
      visible: false,
    })
  }

  start = () => {
    this.setState({ loading: true })
    // ajax request after empty completing
    setTimeout(() => {
      this.setState({
        selectedRowKeys: [],
        loading: false,
        selectedItems: [],
      })
    }, 500)
  }

  onSelectChange = selectedRowKeys => {
    console.log('selectedRowKeys changed: ', selectedRowKeys)
    this.setState({ selectedRowKeys })
  }

  handleChange = selectedItems => {
    this.setState({ selectedItems })
  }

  render() {
    const { selectedItems } = this.state
    const { visitor, tag } = this.props
    const { tags } = tag
    const { visitors } = visitor
    // const filteredOptions = tags.filter(o => !selectedItems.includes(o))
    const { loading, selectedRowKeys } = this.state
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    }
    // const OPTIONS = [{
    //   id: "f8951a90-f2e3-11e9-981c-8778a1ca59d1",
    //   name: "normal",
    //   owner_id: "4e88a811-3ac3-4e9b-a98a-9e5364011059",
    //   status: true
    // },
    // {
    //   id: "fc159630-f21c-11e9-95e6-292d73d9c38a",
    //   name: "vip",
    //   owner_id: "4e88a811-3ac3-4e9b-a98a-9e5364011059",
    //   status: true
    // },];

    const hasSelected = selectedRowKeys.length > 0
    const hasSelectedTag = selectedItems.length > 0
    const columns = [
      {
        title: 'Email',
        width: 200,
        dataIndex: 'email',
        key: '1',
        fixed: 'left',
      },
      {
        title: 'First name',
        dataIndex: 'first_name',
        key: '6',
        width: 150,
      },
      {
        title: 'Last name',
        dataIndex: 'last_name',
        key: '7',
        width: 150,
      },
      {
        title: 'Address',
        dataIndex: 'address',
        key: '2',
        width: 150,
      },
      {
        title: 'Email Marketing',
        dataIndex: 'email',
        key: '3',
        width: 150,
      },
      {
        title: 'Tags',
        dataIndex: 'tags',
        key: '4',
        width: 150,
        render: () => (
          <div>
            <Tag color="green">vip</Tag>
            <Tag color="cyan">cyan</Tag>
          </div>
        ),
      },
      {
        title: 'Lastest changed',
        dataIndex: 'last_modified_date',
        key: '5',
        width: 150,
        format: 'D MMM YYYY',
        // render: () => visitors.map((item) => {
        //   return (
        //     <Moment key={item.id} format="D MMM YYYY" withTitle>
        //       {item.last_modified_date}
        //     </Moment>);
        // })
      },
    ]
    // eslint-disable-next-line
    return (
      <div>
        <div>
          <Modal
            title="Basic Modal"
            // eslint-disable-next-line
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
          >
            <p>Some contents...</p>
            <p>Some contents...</p>
            <p>Some contents...</p>
          </Modal>
        </div>
        <Row style={{ marginBottom: 16 }}>
          <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
            Reload
          </Button>
          <Dropdown.Button
            style={{ marginLeft: '3px' }}
            disabled={!hasSelected}
            onClick={handleButtonClick}
            overlay={menu}
            icon={<Icon type="down" />}
          >
            Action
          </Dropdown.Button>
          <Select
            disabled={!hasSelected}
            mode="multiple"
            placeholder="Add Tag"
            value={selectedItems}
            onChange={this.handleChange}
            style={{ width: '30%', marginLeft: '3px' }}
          >
            {!tags
              ? null
              : tags.map(item => (
                  <Select.Option key={item.id} value={item.name}>
                    {item.name}
                  </Select.Option>
                ))}
          </Select>
          <Button
            style={{ marginLeft: '3px' }}
            type="primary"
            onClick={openNotification}
            disabled={!hasSelectedTag}
            loading={loading}
          >
            Add
          </Button>
          <span style={{ marginLeft: 8 }}>
            {hasSelected ? ` ${selectedRowKeys.length} Selected` : ''}
          </span>
          <Button type="primary" onClick={this.showModal} style={{ float: 'right' }}>
            <Icon type="user-add" />
            Add Customer
          </Button>
        </Row>
        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={visitors}
          scroll={{ x: 1250, y: 600 }}
          rowKey={record => record.id}
        />
      </div>
    )
  }
}

// ReactDOM.render(
//   <Table columns={columns} dataSource={data} scroll={{ x: 1500, y: 300 }} />
// );
export default Customers
