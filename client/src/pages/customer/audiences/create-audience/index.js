import React from 'react'
import { Button, Col, Row, Input, Form, Icon, Select } from 'antd'
import { Link } from 'react-router-dom'
import styles from './style.module.scss'

const { Option, OptGroup } = Select
let id = 0
// const value = 0
function onBlur() {
  console.log('blur')
}

function onFocus() {
  console.log('focus')
}

class CreateAudience extends React.Component {
  remove = k => {
    const { form } = this.props
    // can use data-binding to get
    const keys = form.getFieldValue('keys')
    // We need at least one passenger
    if (keys.length === 0) {
      return
    }
    // can use data-binding to set
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    })
  }

  add = () => {
    const { form } = this.props
    // can use data-binding to get
    const keys = form.getFieldValue('keys')
    const nextKeys = keys.concat((id += 1))
    // can use data-binding to set
    // important! notify form to detect changes
    form.setFieldsValue({
      keys: nextKeys,
    })
  }

  handleSubmit = e => {
    e.preventDefault()
    // eslint-disable-next-line
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { keys, names } = values
        console.log('Received values of form: ', values)
        console.log('Merged values:', keys.map(key => names[key]))
      }
    })
  }

  // onChange = e => {
  //   this.setState({ value: e })
  //   // alert(e)
  // }

  render() {
    // const { value } = this.state
    // eslint-disable-next-line
    const { getFieldDecorator, getFieldValue } = this.props.form
    // const formItemLayoutWithOutLabel = {
    //   wrapperCol: {
    //     xs: { span: 24, offset: 0 },
    //     sm: { span: 20, offset: 0 },
    //   },
    // }
    getFieldDecorator('keys', { initialValue: [] })
    const keys = getFieldValue('keys')
    const formItems = keys.map(k => (
      <div className={styles.formFilter}>
        <Select
          // showSearch
          style={{ width: 400, margin: '10px 10px' }}
          placeholder="Select a condition"
          optionFilterProp="children"
          onFocus={onFocus}
          // onBlur={onBlur}
          // onChange={this.onChange}

          // filterOption={(input, option) =>
          //   option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          // }
        >
          {/* <OptGroup className={styles.optGroup} label="Tags"> */}
          <Option value="2">What someone has done (or not done) </Option>
          <Option value="3">If someone is or is not in suppressed</Option>
          {/* </OptGroup> */}
          {/* <OptGroup label="Audience"> */}
          <Option value="5">Predictive analytics about someone</Option>
          <Option value="6">If someone is in or is not in a list</Option>
          {/* </OptGroup> */}
        </Select>
        <Form.Item required={false} key={k}>
          {
            <Select
              showSearch
              style={{ width: '30%', margin: '10px 10px' }}
              placeholder="Select a 3"
              optionFilterProp="children"
              onFocus={onFocus}
              onBlur={onBlur}
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              <OptGroup className={styles.optGroup} label="Tags">
                <Option value="2">Vip</Option>
                <Option value="3">Classic</Option>
              </OptGroup>
              <OptGroup label="Audience">
                <Option value="5">Vip</Option>
                <Option value="6">Classic</Option>
              </OptGroup>
            </Select>
          }
          {
            <Select
              showSearch
              style={{ width: '30%', margin: '10px 10px' }}
              placeholder="Select a condition"
              optionFilterProp="children"
              onFocus={onFocus}
              onBlur={onBlur}
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              <OptGroup className={styles.optGroup} label="Tags">
                <Option value="2">Vip</Option>
                <Option value="3">Classic</Option>
              </OptGroup>
              <OptGroup label="Audience">
                <Option value="5">Vip</Option>
                <Option value="6">Classic</Option>
              </OptGroup>
            </Select>
          }
          {keys.value === 5 ? (
            <Select
              showSearch
              style={{ width: '30%', margin: '10px 10px' }}
              placeholder="Select a SON"
              optionFilterProp="children"
              onFocus={onFocus}
              onBlur={onBlur}
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              <OptGroup className={styles.optGroup} label="Tags">
                <Option value="2">Vip</Option>
                <Option value="3">Classic</Option>
              </OptGroup>
              <OptGroup label="Audience">
                <Option value="5">Vip</Option>
                <Option value="6">Classic</Option>
              </OptGroup>
            </Select>
          ) : null}
          {keys.length > 0 ? (
            <Icon
              className="dynamic-delete-button"
              type="minus-circle-o"
              onClick={() => this.remove(k)}
            />
          ) : null}
        </Form.Item>
      </div>
    ))
    return (
      <Form>
        <div className={styles.border}>
          <h5 style={{ fontWeight: 'bold', margin: '3px' }}>Name</h5>
          <div className={styles.top}>
            <Input className={styles.txtAuName} placeholder="Audience Name" />
            <span className={styles.txtDesctiption}>
              * Now you can generate personalized messages based on user behavior
            </span>
          </div>

          <div className={styles.body}>
            <h5 style={{ fontWeight: 'bold' }}>Definition</h5>
            {formItems}
            <Form.Item className={styles.formItem}>
              <Button type="dashed" onClick={this.add} className={styles.btnAdd}>
                AND
              </Button>
            </Form.Item>
          </div>
        </div>
        <div className={styles.bottom}>
          <Row className={styles.btnPreview}>
            <Col style={{ float: 'right' }}>
              <Link to="">
                <Button type="primary">Preview</Button>
              </Link>
              <Link to="./">
                <Button type="default">Cancel</Button>
              </Link>
            </Col>
          </Row>
        </div>
      </Form>
    )
  }
}
const CreateAudiences = Form.create()(CreateAudience)
export default CreateAudiences
