import React from 'react'
import { Button, Icon, Input, Table, Row } from 'antd'
import { Link } from 'react-router-dom'
// import styles from './style.module.scss'
import Highlighter from 'react-highlight-words'

const ButtonGroup = Button.Group
// const message = Modal;
const data = [
  {
    key: 1,
    audience: 'audience-name-1',
    edited_date: 'Fri, September 27th',
  },
  {
    key: 2,
    audience: 'audience-name-2',
    edited_date: 'Fri, September 27th',
  },
  {
    key: 3,
    audience: 'audience-name-3',
    edited_date: 'Fri, September 27th',
  },
]

// function handleButtonClick(e) {
//   message.info('Click on left button.');
//   console.log('click left button', e);
// }

const columns = [
  {
    title: 'Audience Name',
    dataIndex: 'audience',
    key: 'audience',
    width: '80%',
    // ...this.getColumnSearchProps('audience'),
    render: audience => (
      <Row>
        <a href="/audience/edit">
          <h5>{audience}</h5>
        </a>
        <p>
          Edited on <span style={{ fontWeight: 'bold' }}> Fri, September 27th </span> at{' '}
        </p>
      </Row>
    ),
  },
  {
    title: 'Action',
    // dataIndex: 'action',
    key: 'action',
    // ...this.getColumnSearchProps('action'),
    render: () => (
      <ButtonGroup>
        <Button type="primary" icon="edit" />
        <Button style={{ marginLeft: '3px' }}>
          <Icon type="mail" />
        </Button>
      </ButtonGroup>
    ),
  },
]

// const { Header, Footer, Sider, Content } = Layout
class Audience extends React.Component {
  state = {
    searchText: '',
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select())
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        // eslint-disable-next-line
        searchWords={[this.state.searchText]}
        autoEscape
        // eslint-disable-next-line
        textToHighlight={text.toString()}
      />
    ),
  })

  handleSearch = (selectedKeys, confirm) => {
    confirm()
    this.setState({ searchText: selectedKeys[0] })
  }

  handleReset = clearFilters => {
    clearFilters()
    this.setState({ searchText: '' })
  }

  render() {
    // const columns = [
    //   {
    //     title: 'Audience Name',
    //     dataIndex: 'name',
    //     key: 'name',
    //     width: '80%',
    //     ...this.getColumnSearchProps('name'),
    //   },
    //   {
    //     title: 'Action',
    //     // dataIndex: 'action',
    //     key: 'action',
    //     // ...this.getColumnSearchProps('action'),
    //     render: () =><ButtonGroup><Button type="primary" icon="edit" /><Button style={{marginLeft:'3px'}}><Icon type="mail" /></Button></ButtonGroup>,
    //   },
    // ]

    const rowSelection = {
      onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows)
      },
      onSelect: (record, selected, selectedRows) => {
        console.log(record, selected, selectedRows)
      },
      onSelectAll: (selected, selectedRows, changeRows) => {
        console.log(selected, selectedRows, changeRows)
      },
    }

    return (
      <div>
        <Row>
          <Link to="audiences/create-audience">
            <Button type="primary" style={{ float: 'right' }}>
              <Icon type="usergroup-add" />
              Create Audience
            </Button>
          </Link>
        </Row>
        <Table columns={columns} rowSelection={rowSelection} dataSource={data} />
      </div>
    )
  }
}

export default Audience
