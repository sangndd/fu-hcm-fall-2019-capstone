import React from 'react'
import { Helmet } from 'react-helmet'

// eslint-disable-next-line no-unused-vars
import { Row, Col, Button, Tabs, Table, Tag, Divider, Input, Form, Modal } from 'antd'
import ViewByStatus from './view-by-status'
import CampaignHomeBody from './main-body'

const CampaignCreateForm = Form.create({ name: 'new_campaign_in_modal' })(
  // eslint-disable-next-line
  class extends React.Component {
    render() {
      const { visible, onCancel, onCreate, form } = this.props
      const { getFieldDecorator } = form
      return (
        <Modal
          visible={visible}
          title="Create Campaign"
          okText="Create"
          onCancel={onCancel}
          onOk={onCreate}
        >
          <Form layout="vertical">
            <Form.Item label="Campaign name">
              {getFieldDecorator('campaign-name', {
                rules: [{ required: true, message: 'Please input the campaign name' }],
              })(<Input />)}
            </Form.Item>
          </Form>
        </Modal>
      )
    }
  },
)

class CampaignHome extends React.Component {
  state = {
    visible: false,
  }

  showModal = () => {
    this.setState({ visible: true })
  }

  handleCancel = () => {
    this.setState({ visible: false })
  }

  handleCreate = () => {
    const { form } = this.formRef.props
    form.validateFields((err, values) => {
      if (err) {
        return
      }

      console.log('Campaign name: ', values)
      form.resetFields()
      // eslint-disable-next-line react/destructuring-assignment
      this.props.history.push('/campaign/edit')
      this.setState({ visible: false })
    })
  }

  saveFormRef = formRef => {
    this.formRef = formRef
  }

  render() {
    return (
      <section className="card">
        <div className="card-body">
          <Helmet title="Campaigns" />
          <Row type="flex" justify="start">
            <Col span={21}>
              <h1>Campaigns</h1>
              <h3>You can manage all campaigns here.</h3>
            </Col>
            <Col span={3}>
              <Button type="primary" size="large" onClick={this.showModal}>
                Create Campaign
              </Button>
              <CampaignCreateForm
                wrappedComponentRef={this.saveFormRef}
                // eslint-disable-next-line react/destructuring-assignment
                visible={this.state.visible}
                onCancel={this.handleCancel}
                onCreate={this.handleCreate}
              />
            </Col>
          </Row>
          <br />
          <br />
          <Row type="flex" justify="start" gutter={20}>
            <Col span={4} style={{ width: 190 }}>
              <div className="card" style={{ height: 25 }}>
                {['']}
              </div>
              {/* <Divider /> */}
              <ViewByStatus />
            </Col>
            <Col span={20}>
              <CampaignHomeBody />
            </Col>
          </Row>
        </div>
      </section>
    )
  }
}

export default CampaignHome
