import React from 'react'
import { Tabs, Row, Col, Divider, Button, Table, Input, Tag } from 'antd'
import { Link } from 'react-router-dom'

const { TabPane } = Tabs
const { Search } = Input
const table1Cols = [
  {
    dataIndex: 'campaign',
    key: 'campaign',
    render: campaignObj => (
      <Row>
        <Link to={`/campaign/edit/${campaignObj[0]}`}>
          <h5>{campaignObj[0]}</h5>
        </Link>
        <div>{campaignObj[1]}</div>
        <p>
          Edited on <span style={{ fontWeight: 'bold' }}>{campaignObj[2]}</span> at{' '}
          <span style={{ fontWeight: 'bold' }}>{campaignObj[3]}</span>
        </p>
      </Row>
    ),
  },
  {
    dataIndex: 'status',
    key: 'status',
    align: 'right',
    render: status => (
      <span>
        {status.map(tag => {
          let color = 'green'
          if (tag === 'draft') {
            color = 'volcano'
          } else if (tag === 'running') {
            color = 'geekblue'
          }
          return (
            <Tag color={color} key={tag} style={{ width: 100, textAlign: 'center' }}>
              {tag.toUpperCase()}
            </Tag>
          )
        })}
      </span>
    ),
  },
  // {
  //   dataIndex: 'action',
  // },
]
const demoData1 = [
  {
    key: 1,
    campaign: ['campaign-name-1', 'segment-name', 'Fri, September 27th', '11:17'],
    status: ['draft'],
  },
  {
    key: 2,
    campaign: ['campaign-name-2', 'segment-name', 'Thu, September 26th', '22:12'],
    status: ['running'],
  },
  {
    key: 3,
    campaign: ['campaign-name-3', 'segment-name', 'Sat, September 28th', '8:00'],
    status: ['completed'],
  },
]

const table2Cols = [
  {
    dataIndex: 'campaign',
    key: 'campaign',
    render: campaignObj => (
      <Row>
        <Link to={`/campaign/edit/${campaignObj[0]}`}>
          <h5>{campaignObj[0]}</h5>
        </Link>
        {/* <div>{campaignObj[1]}</div> */}
        <p>
          Edited on <span style={{ fontWeight: 'bold' }}>{campaignObj[1]}</span> at{' '}
          <span style={{ fontWeight: 'bold' }}>{campaignObj[2]}</span>
        </p>
      </Row>
    ),
  },
  {
    dataIndex: 'status',
    key: 'status',
    align: 'center',
    render: status => (
      <span>
        {status.map(tag => {
          let color
          let actionButton
          if (tag === 'running') {
            color = 'geekblue'
            actionButton = <Button size="large" shape="round" type="danger" icon="stop" />
          } else {
            color = 'red'
            actionButton = <Button size="large" shape="round" type="primary" icon="play-circle" />
          }
          return (
            <Row type="flex" justify="space-between">
              <Col span={6}>
                <Tag color={color} key={tag} style={{ width: 100, textAlign: 'center' }}>
                  {tag.toUpperCase()}
                </Tag>
              </Col>
              <Col span={6}>{actionButton}</Col>
            </Row>
          )
        })}
      </span>
    ),
  },
  // {
  //   dataIndex: 'action',
  //   key: 'action',
  // },
]
const demoData2 = [
  {
    key: 1,
    campaign: ['Abandoned Cart', 'Fri, September 27th', '11:17'],
    status: ['running'],
  },
  {
    key: 2,
    campaign: ['Welcome new subscribers', 'Thu, September 26th', '22:12'],
    status: ['running'],
  },
  {
    key: 3,
    campaign: ['Thank first-time customer', 'Sat, September 28th', '8:00'],
    status: ['stopped'],
  },
]

class CampaignHomeBody extends React.Component {
  state = {
    selectedRowKeys: [],
  }

  onSelectChange = selectedRowKeys => {
    this.setState({ selectedRowKeys })
  }

  deselectAll = () => {
    this.setState({ selectedRowKeys: [] })
  }

  render() {
    const { selectedRowKeys } = this.state
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    }
    const hasSelected = selectedRowKeys.length > 0
    return (
      <Tabs defaultActiveKey="1">
        <TabPane tab={<h5>Regular Campaign</h5>} key="1">
          <div>
            <Row type="flex" justify="start">
              <Col span={10}>
                <Divider type="vertical" />
                <Button type="primary" disabled={!hasSelected} onClick={this.deselectAll}>
                  Deselect All
                </Button>
                <Button type="danger" disabled={!hasSelected} style={{ marginLeft: 10 }}>
                  Delete
                </Button>
                <Divider type="vertical" />
                <span style={{ marginLeft: 8 }}>
                  {hasSelected ? `${selectedRowKeys.length} selected` : ''}
                </span>
              </Col>
              <Col span={10} offset={4}>
                <Search
                  placeholder="Find a campaign by name or type"
                  onSearch={value => console.log(value)}
                  style={{ width: 300, float: 'right' }}
                />
              </Col>
            </Row>
            <Divider />
            <Table
              showHeader={false}
              rowSelection={rowSelection}
              columns={table1Cols}
              dataSource={demoData1}
            />
          </div>
        </TabPane>
        <TabPane tab={<h5>Automation</h5>} key="2">
          <Table
            showHeader={false}
            rowSelection={rowSelection}
            columns={table2Cols}
            dataSource={demoData2}
          />
        </TabPane>
      </Tabs>
    )
  }
}

export default CampaignHomeBody
