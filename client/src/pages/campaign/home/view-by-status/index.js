import React from 'react'
import { Menu } from 'antd'

class ViewByStatus extends React.Component {
  render() {
    return (
      <Menu defaultSelectedKeys={['1']} mode="inline">
        <Menu.ItemGroup title={<h5 style={{ fontWeight: 'bold' }}>View by Status</h5>}>
          <Menu.Item key="1">
            <span>All</span>
          </Menu.Item>
          <Menu.Item key="2">
            <span>Running</span>
          </Menu.Item>
          <Menu.Item key="3">
            <span>Draft</span>
          </Menu.Item>
          <Menu.Item key="4">
            <span>Completed</span>
          </Menu.Item>
        </Menu.ItemGroup>
      </Menu>
    )
  }
}

export default ViewByStatus
