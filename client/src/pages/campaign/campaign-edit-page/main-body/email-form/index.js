import React from 'react'
import { Editor } from 'react-draft-wysiwyg'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { Form, Input, Button } from 'antd'
import styles from '../style.module.scss'

const FormItem = Form.Item

@Form.create()
class EmailForm extends React.Component {
  render() {
    const { form } = this.props

    return (
      <Form className="mt-3" layout="vertical">
        <div className="form-group">
          <FormItem label="Subject">
            {form.getFieldDecorator('subject')(<Input placeholder="Email subject" />)}
          </FormItem>
        </div>

        <div className="form-group">
          <FormItem label="Content">
            {form.getFieldDecorator('content')(
              <div className={styles.editor}>
                <Editor />
              </div>,
            )}
          </FormItem>
        </div>

        <FormItem>
          <div className={styles.submit}>
            <span className="mr-3">
              <Button type="primary" htmlType="submit">
                Save
              </Button>
            </span>
            <Button type="danger" htmlType="submit">
              Discard
            </Button>
          </div>
        </FormItem>
      </Form>
    )
  }
}

export default EmailForm
