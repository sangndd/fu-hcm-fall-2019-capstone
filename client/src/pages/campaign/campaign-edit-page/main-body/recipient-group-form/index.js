import React from 'react'
import { Form, Select, Button } from 'antd'

const FormItem = Form.Item
const { Option } = Select

@Form.create()
class RecipientGroupForm extends React.Component {
  render() {
    const { form } = this.props

    return (
      <div>
        <Form layout="vertical">
          <FormItem label="Audience">
            {form.getFieldDecorator('audience')(
              <Select style={{ width: 700 }} mode="multiple" placeholder="Choose audience">
                <Option value="1">Red</Option>
                <Option value="2">Green</Option>
                <Option value="3">Blue</Option>
                <Option value="4">Red</Option>
                <Option value="5">Green</Option>
                <Option value="6">Blue</Option>
                <Option value="7">Red</Option>
                <Option value="8">Green</Option>
                <Option value="9">Blue</Option>
                <Option value="10">Red</Option>
                <Option value="11">Green</Option>
                <Option value="12">Blue</Option>
                <Option value="13">Red</Option>
                <Option value="14">Green</Option>
                <Option value="15">Blue</Option>
                <Option value="16">Red</Option>
                <Option value="17">Green</Option>
                <Option value="18">Blue</Option>
                <Option value="19">Red</Option>
                <Option value="20">Green</Option>
                <Option value="21">Blue</Option>
                <Option value="22">Red</Option>
                <Option value="23">Green</Option>
                <Option value="24">Blue</Option>
              </Select>,
            )}
          </FormItem>
          <FormItem>
            <Button type="primary" style={{ marginLeft: 10, marginRight: 10 }}>
              Save
            </Button>
            <Button type="ghost">Cancel</Button>
          </FormItem>
        </Form>
      </div>
    )
  }
}

export default RecipientGroupForm
