import React from 'react'
import { Form, Input, Button, Select } from 'antd'

const { Option } = Select

const FormItem = Form.Item

@Form.create()
class SenderInfoForm extends React.Component {
  render() {
    const { form } = this.props

    return (
      <div>
        <Form layout="vertical">
          <FormItem label="Name">
            {form.getFieldDecorator('name')(
              <Input style={{ width: 500 }} placeholder="Sender's name / Sender's title" />,
            )}
          </FormItem>
          <FormItem label="Email">
            {form.getFieldDecorator('email')(
              <Select style={{ width: 500 }} showSearch showArrow placeholder="Choose an email">
                <Option value="1">abc1@gmail.com</Option>
                <Option value="2">abc2@gmail.com</Option>
                <Option value="3">abc3@gmail.com</Option>
                <Option value="4">abc4@gmail.com</Option>
                <Option value="5">abc5@gmail.com</Option>
                <Option value="6">abc6@gmail.com</Option>
              </Select>,
            )}
          </FormItem>
          <FormItem>
            <Button type="primary" style={{ marginLeft: 10, marginRight: 10 }}>
              Save
            </Button>
            <Button type="ghost">Cancel</Button>
          </FormItem>
        </Form>
      </div>
    )
  }
}

export default SenderInfoForm
