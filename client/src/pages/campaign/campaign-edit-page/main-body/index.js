import React from 'react'
import { Steps, Icon, Row, Button } from 'antd'
import SenderInfoForm from './sender-info-form'
import RecipientGroupForm from './recipient-group-form'
import EmailForm from './email-form'

const { Step } = Steps

class CampaignEditPageMainBody extends React.Component {
  state = {
    current: 0,
  }

  onChange = current => {
    console.log('onChange:', current)
    this.setState({ current })
  }

  render() {
    const { current } = this.state
    return (
      <div>
        <Steps current={current} onChange={this.onChange} direction="vertical">
          <Step
            status="finish"
            title={<h3>Sender Information</h3>}
            icon={<i className="icmn icmn-user-tie" />}
            description={
              current === 0 ? (
                <div className="card-body">
                  <SenderInfoForm />
                </div>
              ) : (
                ['']
              )
            }
          />
          <Step
            status="finish"
            title={<h3>Recipient Group</h3>}
            icon={<i className="icmn icmn-users" />}
            description={
              current === 1 ? (
                <div className="card-body">
                  <RecipientGroupForm />
                </div>
              ) : (
                ['']
              )
            }
          />
          <Step
            status="finish"
            title={<h3>Email Content</h3>}
            icon={<Icon type="mail" theme="filled" />}
            description={
              current === 2 ? (
                <div className="card-body">
                  <Row justify="end" type="flex">
                    <Button type="primary">
                      <span>
                        <i style={{ marginRight: 10 }} className="icmn icmn-file-text2" />
                        Choose a Template
                      </span>
                    </Button>
                  </Row>
                  <EmailForm />
                </div>
              ) : (
                ['']
              )
            }
          />
        </Steps>
      </div>
    )
  }
}

export default CampaignEditPageMainBody
