import React from 'react'
import { Helmet } from 'react-helmet'
import CampaignEditPageHeader from './header'
import CampaignEditPageMainBody from './main-body'

class CampaignEditPage extends React.Component {
  render() {
    return (
      <section className="card">
        <Helmet title="Create Campaign" />
        <div className="card-body">
          <CampaignEditPageHeader />
          <CampaignEditPageMainBody />
        </div>
      </section>
    )
  }
}

export default CampaignEditPage
