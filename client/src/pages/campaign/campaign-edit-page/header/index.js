import React from 'react'
import { PageHeader, Tag, Descriptions, Button } from 'antd'
import { Link } from 'react-router-dom'

const campaignStatus = ['draft']

class CampaignEditPageHeader extends React.Component {
  render() {
    return (
      <PageHeader
        title={<h2 className="font-weight-bold">campaign-name</h2>}
        subTitle={
          <Button type="ghost" size="small">
            edit name
          </Button>
        }
        extra={[
          <Button type="link" onClick={() => window.history.back()}>
            Finish later
          </Button>,
          <Link to="/campaign/schedule">
            <Button>Schedule</Button>
          </Link>,
          <Link to="/campaign">
            <Button type="primary">Send</Button>
          </Link>,
        ]}
        tags={
          <span>
            {campaignStatus.map(tag => {
              let color = 'green'
              if (tag === 'draft') {
                color = 'volcano'
              } else if (tag === 'running') {
                color = 'geekblue'
              }
              return (
                <Tag color={color} key={tag}>
                  {tag.toUpperCase()}
                </Tag>
              )
            })}
          </span>
        }
      >
        <Descriptions size="small" column={1}>
          <Descriptions.Item label="Last modified">09/20/2019 9:00</Descriptions.Item>
        </Descriptions>
      </PageHeader>
    )
  }
}

export default CampaignEditPageHeader
