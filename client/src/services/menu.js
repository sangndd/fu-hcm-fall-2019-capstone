export async function getLeftMenuData() {
  return [
    {
      title: 'Theme Settings',
      key: 'settings',
      icon: 'icmn icmn-cog utils__spin-delayed--pseudo-selector',
    },
    {
      divider: true,
    },
    {
      title: 'Dashboard',
      key: 'dashboardAlpha',
      url: '/dashboard/alpha',
      icon: 'icmn icmn-home',
    },
    {
      divider: true,
    },
    {
      title: 'Campaign',
      key: 'emmCampaign',
      icon: 'icmn icmn-bullhorn',
      url: '/campaign',
    },
    {
      divider: true,
    },
    {
      title: 'Customer',
      key: 'customer',
      icon: 'icmn icmn-users',
      children: [
        {
          title: 'View customers',
          key: 'viewCustomers',
          url: '/customer/customers',
        },
        {
          title: 'Audiences',
          key: 'audiences',
          url: '/customer/audiences',
        },
        {
          title: 'Tags',
          key: 'tags',
          url: '/customer/tags',
        },
      ],
    },
    {
      divider: true,
    },
    {
      title: 'Utilities',
      key: 'utilities',
      icon: 'icmn icmn-briefcase',
      children: [
        {
          title: 'Gallery',
          key: 'gallery',
          icon: 'icmn icmn-images',
          url: '/util/gallery',
        },
        {
          title: 'Email Templates',
          key: 'emailTemplates',
          icon: 'icmn icmn-file-text',
          url: '/util/email-templates',
        },
      ],
    },
  ]
}
export async function getTopMenuData() {
  return [
    {
      title: 'Theme Settings',
      key: 'settings',
      icon: 'icmn icmn-cog utils__spin-delayed--pseudo-selector',
    },
    {
      title: 'Dashboard',
      key: 'dashboardAlpha',
      url: '/dashboard/alpha',
      icon: 'icmn icmn-home',
    },
    {
      title: 'Campaign',
      key: 'emmCampaign',
      icon: 'icmn icmn-bullhorn',
      url: '/campaign',
    },
    {
      title: 'Customer',
      key: 'customer',
      icon: 'icmn icmn-users',
      children: [
        {
          title: 'View customers',
          key: 'viewCustomers',
          url: '/customer/lists',
        },
        {
          title: 'Segments',
          key: 'segments',
          url: '/customer/segments',
        },
        {
          title: 'Tags',
          key: 'tags',
          url: '/customer/tags',
        },
      ],
    },
    {
      title: 'Utilities',
      key: 'utilities',
      children: [
        {
          title: 'Gallery',
          key: 'gallery',
          icon: 'icmn icmn-images',
          url: '/util/gallery',
        },
        {
          title: 'Email Templates',
          key: 'emailTemplates',
          icon: 'icmn icmn-file-text',
          url: '/util/email-templates',
        },
      ],
    },
  ]
}
