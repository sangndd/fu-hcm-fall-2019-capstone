import axios from 'axios'

// eslint-disable-next-line import/prefer-default-export
export async function getVisitors(shopUrl) {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  }
  const url = `http://localhost:3000/api/visitors/${shopUrl}`
  return axios.get(url, config).then(result => {
    console.log(result.data)
    return result.data
  })
}
