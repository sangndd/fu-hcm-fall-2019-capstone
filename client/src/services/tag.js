import axios from 'axios'

// eslint-disable-next-line import/prefer-default-export
export async function getTags() {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  }
  const url = `http://localhost:3000/api/tags`
  return axios.get(url, config).then(result => {
    console.log(result.data)
    return result.data
  })
}
